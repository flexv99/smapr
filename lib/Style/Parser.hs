{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Style.Parser where

import qualified Data.Aeson as A
import Data.Colour
import Data.Colour.RGBSpace.HSL
import Data.Colour.SRGB
import Data.List (singleton)
import Data.Scientific (isFloating, toRealFloat)
import qualified Data.Text.Internal.Lazy as T
import qualified Data.Text.Lazy as T
import qualified Data.Vector as V
import Data.Void
import GHC.Generics (Generic)
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

-- Void: The type for custom error messages. We have none, so use `Void`.
-- T.Text: The input stream type.
type Parser = Parsec Void T.Text

-- Here are defined the types being used in the style spec
-- reference: https://github.com/maplibre/maplibre-style-spec/blob/main/src/expression/types.ts#L133
-- Docs: https://maplibre.org/maplibre-style-spec/expressions/#types

type Color = AlphaColour Double

data INum = SInt Int | SDouble Double deriving (Show, Generic, Eq, Ord)

instance Num INum where
  (SInt a) + (SInt b) = SInt (a + b)
  (SDouble a) + (SDouble b) = SDouble (a + b)
  (SInt a) + (SDouble b) = SDouble (fromIntegral a + b)
  (SDouble a) + (SInt b) = SDouble (a + fromIntegral b)

  (SInt a) * (SInt b) = SInt (a * b)
  (SDouble a) * (SDouble b) = SDouble (a * b)
  (SInt a) * (SDouble b) = SDouble (fromIntegral a * b)
  (SDouble a) * (SInt b) = SDouble (a * fromIntegral b)

  abs (SInt a) = SInt (abs a)
  abs (SDouble a) = SDouble (abs a)

  signum (SInt a) = SInt (signum a)
  signum (SDouble a) = SDouble (signum a)

  fromInteger a = SInt (fromInteger a)

  negate (SInt a) = SInt (negate a)
  negate (SDouble a) = SDouble (negate a)

instance Fractional INum where
  (SInt a) / (SInt b) = SDouble (fromIntegral a / fromIntegral b)
  (SDouble a) / (SDouble b) = SDouble (a / b)
  (SInt a) / (SDouble b) = SDouble (fromIntegral a / b)
  (SDouble a) / (SInt b) = SDouble (a / fromIntegral b)

  fromRational a = SDouble (fromRational a)

instance Floating INum where
  pi = SDouble pi

  exp (SInt a) = SDouble (exp (fromIntegral a))
  exp (SDouble a) = SDouble (exp a)

  log (SInt a) = SDouble (log (fromIntegral a))
  log (SDouble a) = SDouble (log a)

  sin (SInt a) = SDouble (sin (fromIntegral a))
  sin (SDouble a) = SDouble (sin a)

  cos (SInt a) = SDouble (cos (fromIntegral a))
  cos (SDouble a) = SDouble (cos a)

  asin (SInt a) = SDouble (asin (fromIntegral a))
  asin (SDouble a) = SDouble (asin a)

  acos (SInt a) = SDouble (acos (fromIntegral a))
  acos (SDouble a) = SDouble (acos a)

  atan (SInt a) = SDouble (atan (fromIntegral a))
  atan (SDouble a) = SDouble (atan a)

  sinh (SInt a) = SDouble (sinh (fromIntegral a))
  sinh (SDouble a) = SDouble (sinh a)

  cosh (SInt a) = SDouble (cosh (fromIntegral a))
  cosh (SDouble a) = SDouble (cosh a)

  asinh (SInt a) = SDouble (asinh (fromIntegral a))
  asinh (SDouble a) = SDouble (asinh a)

  acosh (SInt a) = SDouble (acosh (fromIntegral a))
  acosh (SDouble a) = SDouble (acosh a)

  atanh (SInt a) = SDouble (atanh (fromIntegral a))
  atanh (SDouble a) = SDouble (atanh a)

divINum :: INum -> INum -> INum
divINum (SInt x) (SInt y)
  | y == 0 = error "Division by zero"
  | otherwise = SInt (x `div` y)
divINum (SDouble x) (SDouble y)
  | y == 0 = error "Division by zero"
  | otherwise = SDouble (x / y)
divINum (SInt x) (SDouble y)
  | y == 0 = error "Division by zero"
  | otherwise = SDouble (fromIntegral x / y)
divINum (SDouble x) (SInt y)
  | y == 0 = error "Division by zero"
  | otherwise = SDouble (x / fromIntegral y)

data SType
  = SNum INum
  | SString T.Text
  | SBool Bool
  | SColor Color
  | SArray [SType]
  | SNull
  deriving (Show, Generic, Eq)

instance A.FromJSON SType where
  parseJSON (A.Number n) =
    if isFloating n
      then pure $ SNum $ SDouble (toRealFloat n)
      else pure $ SNum $ SInt (round n)
  parseJSON (A.Bool b) = pure $ SBool b
  parseJSON (A.Array a) = SArray <$> traverse A.parseJSON (V.toList a)
  parseJSON a =
    A.withText
      "SType"
      ( \v ->
          case parse pAtom "" (T.fromStrict v) of
            Left err -> fail $ errorBundlePretty err
            Right res -> return res
      )
      a

--- HELPERS

exprBaseP :: T.Text -> Parser a -> Parser a
exprBaseP i rest = betweenSquareBrackets $ do
  _ <- pKeyword i
  _ <- char ',' >> space
  rest

-- the space consumer
sc :: Parser ()
sc = L.space space1 (L.skipLineComment "//") (L.skipBlockComment "/*" "*/")

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: T.Text -> Parser T.Text
symbol = L.symbol sc

-- parse snake case property names
snakeCaseChar :: Parser Char
snakeCaseChar = alphaNumChar <|> char '_'

skipComma :: Parser a -> Parser a
skipComma = L.lexeme (skipMany (spaceChar <|> char ','))

betweenBrackets :: (Token s ~ Char, MonadParsec e s m) => m a -> m a
betweenBrackets = between (char '(' >> space) (char ')' >> space)

betweenSquareBrackets :: (Token s ~ Char, MonadParsec e s m) => m a -> m a
betweenSquareBrackets = between (char '[' >> space) (char ']' >> space)

betweenDoubleQuotes :: (Token s ~ Char, MonadParsec e s m) => m a -> m a
betweenDoubleQuotes = between (char '"' >> space) (char '"' >> space)

numToDouble :: INum -> Double
numToDouble (SDouble d) = d
numToDouble (SInt i) = fromIntegral i

--- PARSER

pKeyword :: T.Text -> Parser T.Text
pKeyword keyword =
  label ("property_key: " ++ T.unpack keyword) $
    betweenDoubleQuotes $
      lexeme (string keyword <* notFollowedBy alphaNumChar)

pString :: Parser T.Text
pString =
  fmap
    T.pack
    ( betweenDoubleQuotes
        (lexeme (many snakeCaseChar) <?> "string")
    )

pBool :: Parser Bool
pBool =
  lexeme
    (False <$ (string "false" *> notFollowedBy alphaNumChar))
    <|> (True <$ (string "true" *> notFollowedBy alphaNumChar))
    <?> "bool"

pInteger :: Parser Int
pInteger = lexeme (L.signed space L.decimal) <?> "integer"

pDouble :: Parser Double
pDouble = lexeme (L.signed space L.float) <?> "float"

pNum :: Parser INum
pNum = try (SDouble <$> pDouble) <|> SInt <$> pInteger

pColor :: Parser Color
pColor = choice $ map try [pHslColor, pHslaColor, pRgbaColor, pRgbColor, pHexColor]

pAtom :: Parser SType
pAtom =
  try $
    choice
      [ numberLitP,
        boolLitP,
        nullP,
        try $ SColor <$> pColor,
        stringLitP,
        arrayLitP
      ]

pNotNullAtom :: Parser SType
pNotNullAtom =
  try $
    choice
      [ numberLitP,
        boolLitP,
        nullP,
        try $ SColor <$> pColor,
        stringLitP,
        arrayLitP
      ]

parserForType :: SType -> Parser SType
parserForType t = case t of
  SNum a -> numP a
  SBool _ -> boolLitP
  SString _ -> stringLitP
  SArray _ -> arrayLitP
  SNull -> nullP
  _ -> pAtom
  where
    numP :: INum -> Parser SType
    numP (SInt _) = SNum <$> intLitP
    numP (SDouble _) = SNum <$> doubleLitP

pArray :: Parser [SType]
pArray =
  label "array" $
    try oneElemArr
      <|> betweenSquareBrackets
        ( do
            firstElem <- pAtom
            _ <- char ',' >> space
            restElems <- parserForType firstElem `sepBy` (char ',' >> space)
            return (firstElem : restElems)
        )
  where
    oneElemArr = betweenSquareBrackets $ do
      val <- pAtom
      return [val]

stringLitP :: Parser SType
stringLitP = SString <$> pString

boolLitP :: Parser SType
boolLitP = SBool <$> pBool

intLitP :: Parser INum
intLitP = SInt <$> pInteger

doubleLitP :: Parser INum
doubleLitP = SDouble <$> pDouble

numberLitINumP :: Parser INum
numberLitINumP = (try doubleLitP <|> intLitP) <?> "number"

numberLitP :: Parser SType
numberLitP = SNum <$> numberLitINumP

arrayLitP :: Parser SType
arrayLitP = SArray <$> pArray

nullP :: Parser SType
nullP = lexeme (SNull <$ (string "null" *> notFollowedBy alphaNumChar)) <?> "null"

-- Color
-- The color type is a color in the sRGB color space. Colors are JSON strings in a variety of permitted formats: HTML-style hex values, RGB, RGBA, HSL, and HSLA. Predefined HTML colors names, like yellow and blue, are also permitted.
-- {
--     "line-color": "#ff0",
--     "line-color": "#ffff00",
--     "line-color": "rgb(255, 255, 0)",
--     "line-color": "rgba(255, 255, 0, 1)",
--     "line-color": "hsl(100, 50%, 50%)",
--     "line-color": "hsla(100, 50%, 50%, 1)",
--     "line-color": "yellow"
-- }

pHslColor :: Parser Color
pHslColor = betweenDoubleQuotes $ do
  _ <- lexeme (string "hsl" <* notFollowedBy alphaNumChar)
  betweenBrackets $ do
    h <- pInt
    _ <- char ',' >> space
    s <- pColorPercentage
    _ <- char ',' >> space
    l <- pColorPercentage
    return $ hslToColor h s l 1
  where
    pInt = lexeme (L.signed space L.decimal)
    pColorPercentage = do
      num <- pInt
      _ <- char '%'
      return num

pHslaColor :: Parser Color
pHslaColor = betweenDoubleQuotes $ do
  _ <- lexeme (string "hsla" <* notFollowedBy alphaNumChar)
  betweenBrackets $ do
    h <- pInt
    _ <- char ',' >> space
    s <- pColorPercentage
    _ <- char ',' >> space
    l <- pColorPercentage
    _ <- char ',' >> space
    hslToColor h s l . numToDouble <$> numberLitINumP
  where
    pInt = lexeme (L.signed space L.decimal)
    pColorPercentage = do
      num <- pInt
      _ <- char '%'
      return num

pRgbColor :: Parser Color
pRgbColor = betweenDoubleQuotes $ do
  _ <- lexeme (string "rgb" <* notFollowedBy alphaNumChar)
  betweenBrackets $ do
    r <- fromIntegral <$> pInteger
    _ <- char ',' >> space
    g <- fromIntegral <$> pInteger
    _ <- char ',' >> space
    b <- fromIntegral <$> pInteger
    return $ sRGB24 r g b `withOpacity` 1

pRgbaColor :: Parser Color
pRgbaColor = betweenDoubleQuotes $ do
  _ <- lexeme (string "rgba" <* notFollowedBy alphaNumChar)
  betweenBrackets $ do
    r <- fromIntegral <$> pInteger
    _ <- char ',' >> space
    g <- fromIntegral <$> pInteger
    _ <- char ',' >> space
    b <- fromIntegral <$> pInteger
    _ <- char ',' >> space
    opacity <- numberLitINumP
    return $ sRGB24 r g b `withOpacity` numToDouble opacity

expandShortHex :: String -> String
expandShortHex hex
  | length hex == 3 = concatMap (replicate 2 . head . singleton) hex
  | otherwise = hex

pHexColor :: Parser Color
pHexColor = betweenDoubleQuotes $ do
  _ <- char '#'
  hexDigits <- try (some hexDigitChar)
  let validLength = length hexDigits == 6 || length hexDigits == 3
  if validLength
    then return $ sRGB24read ("#" <> expandShortHex hexDigits) `withOpacity` 1
    else fail "Invalid hex color code length"

hslToColor :: Double -> Double -> Double -> Double -> Color
hslToColor h s l o = sRGB (channelRed rgb) (channelGreen rgb) (channelBlue rgb) `withOpacity` o
  where
    rgb = hsl h (s / 100) (l / 100)

--------------------------------------------------------------------------------
-- Helpers
--------------------------------------------------------------------------------

showSColor :: Color -> String
showSColor a = sRGB24show $ pureColor a
  where

pureColor :: (Ord a, Fractional a) => AlphaColour a -> Colour a
pureColor ac
  | alphaChannel ac > 0 = darken (recip $ alphaChannel ac) (ac `over` black)
  | otherwise = error "transparent has no pure colour"

tuplifyWithFallback :: [a] -> ([(a, a)], a)
tuplifyWithFallback [] = error "no fallback value"
tuplifyWithFallback [x] = ([], x)
tuplifyWithFallback (x : y : xs) =
  let (tuples, lastElem) = tuplifyWithFallback xs
   in ((x, y) : tuples, lastElem)
